/*
The provider block is used to configure the named provider, in this instance the Azure provider (azurerm). 
The Azure provider is responsible for creating and managing resources on Azure, and for all other interactions with Azure including authentication.
*/

/*
The version argument is optional, but recommended. 
It is used to constrain the provider to a specific version or a range of versions in order to prevent downloading a new provider that may possibly contain breaking changes. 
If the version isn't specified, Terraform will automatically download the most recent provider during initialization. 
*/

provider "azurerm" {
    version = "~>1.32.0"
    subscription_id = "xxxxxxxxxxxxxxxxx"
    tenant_id       = "xxxxxxxxxxxxxxxxx"
    client_id       = "xxxxxxxxxxxxxxxxx"
    client_secret   = "xxxxxxxxxxxxxxxxx"
}


/*
Authentication

Authentication is configured in the provider block. 
The following example shows a configuration for authentication to Azure using a managed identity

https://www.terraform.io/docs/providers/azurerm/index.html#authenticating-to-azure
Terraform supports a number of different methods for authenticating to Azure:

    Authenticating to Azure using the Azure CLI
    Authenticating to Azure using Managed Service Identity
    Authenticating to Azure using a Service Principal and a Client Certificate
    Authenticating to Azure using a Service Principal and a Client Secret


We recommend using either a Service Principal or Managed Service Identity when running Terraform non-interactively (such as when running Terraform in a CI server) 
- and authenticating using the Azure CLI when running Terraform locally.
*/

/*
A resource block defines the desired state for a given resource within the infrastructure. 
A resource can be a physical component such as a network interface, or it can be a logical resource such as an application.
*/
resource "azurerm_resource_group" "rg" {
    name     = "HC-JSP-ResourceGroup"
    location = "koreacentral"

    tags = {
        Environment = "Terraform Getting Started"
        Team = "CBO"   
    }
}

