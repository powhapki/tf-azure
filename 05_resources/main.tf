/*
The provider block is used to configure the named provider, in this instance the Azure provider (azurerm). 
The Azure provider is responsible for creating and managing resources on Azure, and for all other interactions with Azure including authentication.
*/

/*
The version argument is optional, but recommended. 
It is used to constrain the provider to a specific version or a range of versions in order to prevent downloading a new provider that may possibly contain breaking changes. 
If the version isn't specified, Terraform will automatically download the most recent provider during initialization. 
*/

provider "azurerm" {
    version = "~>1.32.0"
    subscription_id = "xxxxxxxxxxxxxxxxx"
    tenant_id       = "xxxxxxxxxxxxxxxxx"
    client_id       = "xxxxxxxxxxxxxxxxx"
    client_secret   = "xxxxxxxxxxxxxxxxx"
}


/*
Authentication

Authentication is configured in the provider block. 
The following example shows a configuration for authentication to Azure using a managed identity

https://www.terraform.io/docs/providers/azurerm/index.html#authenticating-to-azure
Terraform supports a number of different methods for authenticating to Azure:

    Authenticating to Azure using the Azure CLI
    Authenticating to Azure using Managed Service Identity
    Authenticating to Azure using a Service Principal and a Client Certificate
    Authenticating to Azure using a Service Principal and a Client Secret


We recommend using either a Service Principal or Managed Service Identity when running Terraform non-interactively (such as when running Terraform in a CI server) 
- and authenticating using the Azure CLI when running Terraform locally.
*/

/*
# Locate the existing custom/golden image
data "azurerm_image" "search" {
  name                = "vm-cicd-poc-image-ubuntu16.04"
  resource_group_name = "CicdPoCResourceGroup"
}
*/


/*
A resource block defines the desired state for a given resource within the infrastructure. 
A resource can be a physical component such as a network interface, or it can be a logical resource such as an application.
*/
resource "azurerm_resource_group" "rg" {
    name     = "HC-JSP-ResourceGroup"
    location = "koreacentral"

    tags = {
        Environment = "Terraform Getting Started"
        Team = "CBO"   
    }
}

# Create virtual network
resource "azurerm_virtual_network" "vnet" {
  name                = "HC-JSP-TFVnet"
  address_space       = ["10.0.0.0/16"]
  location            = "koreacentral"
  resource_group_name = azurerm_resource_group.rg.name
}



# Create subnet
resource "azurerm_subnet" "subnet" {
  name                 = "HC-JSP-TFSubnet"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefix       = "10.0.1.0/24"
}

# Create public IP
resource "azurerm_public_ip" "publicip" {
  name                = "HC-JSP-TFPublicIP"
  location            = "koreacentral"
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Static"
}


# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg" {
  name                = "HC-JSP-TFNSG"
  location            = "koreacentral"
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# Create network interface
resource "azurerm_network_interface" "nic" {
  name                      = "HC-JSP-NIC"
  location                  = "koreacentral"
  resource_group_name       = azurerm_resource_group.rg.name
  network_security_group_id = azurerm_network_security_group.nsg.id

  ip_configuration {
    name                          = "HC-JSP-NICConfg"
    subnet_id                     = azurerm_subnet.subnet.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip.id
  }
}

# Create a Linux virtual machine
resource "azurerm_virtual_machine" "vm" {
  name                  = "HC-JSP-TFVM"
  location              = "koreacentral"
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic.id]
  vm_size               = "Standard_DS1_v2"

/* with customizred image
  storage_image_reference {
    id = data.azurerm_image.search.id
  }

  storage_os_disk {
    name              = "vm-cicd-poc-ubuntu16.04-OS"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
*/
  storage_os_disk {
    name              = "HC-JSP-OsDisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04.0-LTS"
    version   = "latest"
  }

  os_profile {
    computer_name  = "HC-JSP-TFVM"
    admin_username = "plankton"
    admin_password = "Password1234!"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }
}
